#pragma once

#include "sequence/listsequence.h"
#include "sequence/arraysequence.h"
#include "zip.h"

using namespace std;

template<class T>
class Polynomial {
  Sequence<T> *coeff;
public:
  Polynomial() = default;

  Polynomial(T* list, size_t size, char a)
  {
    if (a == 'D') { coeff = (Sequence<T>*) new ArraySequence(list, size); }
    if (a == 'L') { coeff = (Sequence<T>*) new ListSequence(list, size); }
  }

  Polynomial(T* list, size_t size, size_t m_size, char a) {
    if (a == 'D') { coeff = (Sequence<T> *)new ArraySequence(list, size, m_size); }
    if (a == 'L') { coeff = (Sequence<T> *)new ListSequence(list, size, m_size); }
  }

  Polynomial(const Polynomial<T> &polynomial) : coeff(polynomial.coeff->Clone()) {};

  explicit Polynomial(Sequence<T> *sequence) : coeff(sequence->Clone()) {};

  auto type() {
    return GetFirst()->type();
  }

  Polynomial<T>* CopyNew(int size) {
    return new Polynomial<T>( coeff->CopyNew(size) );
  }

  int GetSize() const {
    return coeff->GetSize();
}

  void Resize(int size) {
    coeff->Resize(size);
  }

  Data<T>* Get(int index) const {
    return (*coeff)[index];
  }

  Data<T>* GetFirst() const {
    return coeff->GetFirst();
  }

  Data<T>* GetLast() const {
    return coeff->GetLast();
  }

  Data<T>* operator[](int index) {
    return coeff->Get(index);
  }

  void Append(Data<T>* item) {
    coeff->Append(item);
  }

  void Prepend(Data<T>* item) {
    coeff->Prepend(item);
  }

  void InsertAt(Data<T>* item, int index) {
    coeff->InsertAt(item, index);
  }

  void Print() const {
    coeff->Print();
  }

  Polynomial<T> *AddPolynomial(Polynomial<T> &polynomial) {
    if (GetSize() >= polynomial.GetSize()){
      auto sum = new Polynomial<T>(coeff);
      for (int i=0; i<polynomial.GetSize(); ++i)
        *sum->coeff->Get(i) + *polynomial.coeff->Get(i);
      return sum;
    }

    else {
      auto sum = new Polynomial<T>(polynomial.coeff);
      for (int i=0; i<GetSize(); ++i)
        *sum->coeff->Get(i) + *coeff->Get(i);
      return sum;
    }
  }

  Polynomial<T> *MulScalar(T scalar) {
    auto *mulled = new Polynomial<T>(coeff);
    for (int i = 0; i < GetSize(); i++) {
      *mulled->coeff->Get(i) * scalar;
    }
    return mulled;
  }

  Polynomial<T> *MulScalar(T*** scalar) {
    auto *mulled = new Polynomial<T>(coeff);
    for (int i = 0; i < GetSize(); i++) {
      *mulled->coeff->Get(i) * scalar;
    }
    return mulled;
  }

  Polynomial<T> *MulPolynomial(Polynomial<T> &polynomial)
  {
    if (GetSize() == 0)
      return this;

    if (polynomial.GetSize() == 0)
      return new Polynomial<T>(polynomial);

    auto new_copy = CopyNew(GetSize() + polynomial.GetSize() - 1);
    auto copy = new Polynomial<T>;

    for (int i = 0; i < polynomial.GetSize(); i++) {
      if (polynomial.type() == "number") { copy = MulScalar(polynomial[i]->Get()); }
      if (polynomial.type() == "matrix") { copy = MulScalar(polynomial[i]->Get_matrix()); }
      for (int j = 0; j < GetSize(); j++) {
        *new_copy->Get(i+j) + *copy->Get(j);
      }
      delete copy;
    }
    return new_copy;
  }


  T CalculatingValue(T value) {
    auto result = 0;
    for (int i = GetSize()-1; i >= 0; --i) {
      result *= value;
      result += Get(i)->Get();
    }
    return result;
  }

  Polynomial<T> *Composition(Polynomial<T> &polynomial)
  {
    auto temp = CopyNew(1);
    for (int i = GetSize()-1; i > 0; i--) {
      *temp->Get(0) + *Get(i);
      temp = temp->MulPolynomial(polynomial);
    }
    *temp->Get(0) + *Get(0);
    return temp;
  }

  void Map(void(*func)(T&)) {
      coeff->Map(func);
  }

  void Where(bool(*func)(T&)) {
    coeff->Where(func);
  }

  Polynomial<T> &operator=(const Polynomial<T> *polynomial) {
    coeff = polynomial->coeff->Clone();
    return *this;
  }

  T& Reduce(Data<T>*(*func)(Data<T>&,Data<T>&)) {
    return coeff->Reduce(func)->Get();
  }

  void zip_unzip_poly(Polynomial<T>* p){
    ZIP<T> z = ZIP(coeff, p->coeff);
    z.ZIPrint();
    z.unzip();
  }

  ~Polynomial() = default;
};
