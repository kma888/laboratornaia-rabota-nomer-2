#pragma once
#include <iostream>

#include "sequence/arraysequence.h"
#include "data/data.h"
#include "data/number.h"
#include "data/matrix.h"

using namespace std;

template<class T>
class DynamicArray {
private:
  size_t size;
  Data<T> **items;

public:
  DynamicArray() : size(0), items(nullptr) {}

  DynamicArray(T *list, size_t size_) {
    items = (Data<T> **)new Nums<T>*[size_];
    size = size_;
    for (int i = 0; i < size; i++) {
      items[i] = new Nums<T>(list[i]);
    }
  }

  DynamicArray(T* matrix, size_t size_, size_t m_size){
    items = (Data<T>**)new Data_matrix<T>*[size_];
    size = size_;
    for (int i=0; i < size; i++){
      items[i] = new Data_matrix<T>(matrix, i*m_size*m_size, m_size);
    }
  }

  DynamicArray(DynamicArray<T> const &a) {
    size = a.GetSize();
    items = new Data<T>*[size];
    for (int i=0; i<size; i++){
      items[i] = a[i]->copy();
    }
  }

  //////////////////////////////////////////////////////////////////////////

  void Set(Data<T>* item, int index) { items[index] = item; }

  void Resize(size_t size_) {
    if (size_ > size) {
      auto new_items = new Data<T>*[size_];
      for (int i = 0; i < size; ++i) {
        new_items[i] = items[i];
      }
      items = new_items;
    }
    else
    {
      for (int i=size_; i<size; i++){
        items[i] = items[i]->copy_new();
      }
    }
    size = size_;
  }

  Data<T>* operator[] (int index) const{
    return items[index];
  }

  Data<T>* Get(int index) const{
    return items[index];
  }

  void where (bool(*func)(T&)){
    for (int i=0; i<size; i++){
      if(!func(items[i]->Get())) {
        for (int j = i; j < size-1; ++j)
          if (j<size-1) { items[j] = items[j + 1]; }
        size--;
        i--;
      }
    }
  }

  Data<T>* reduce(Data<T>*(*func)(Data<T>&,Data<T>&)){
    auto new_data = Get(0)->copy_new();
    for (auto i=0; i<size; ++i){
      new_data = func(*new_data, *Get(i));
    }
    return new_data;
  }

  int GetSize() const { return size; }
};