#pragma once
#include "data/data.h"

template<class T>
class Sequence {
public:
  virtual int GetSize() const = 0;

  virtual void Resize (int size) = 0;

  virtual void Set(Data<T>* item, int index) = 0;

  virtual Data<T>* Get(int index) const = 0;

  virtual Data<T>* GetFirst() const = 0;

  virtual Data<T>* GetLast() const = 0;

  virtual Data<T>* operator[](int index) = 0;

  virtual void Append(Data<T> *item) = 0;

  virtual void Prepend(Data<T> *item) = 0;

  virtual void InsertAt(Data<T> *item, int index) = 0;

  virtual void Print() = 0;

  virtual Sequence<T> *Concat(Sequence<T> &list) = 0;

  virtual Sequence<T> *GetSubsequence(int startIndex, int endIndex) = 0;

  virtual Sequence<T> *Clone() = 0;

  virtual Sequence<T>* CopyNew(int size) = 0;

  virtual void Map(void(*func)(T&)) = 0;

  virtual void Where(bool(*func)(T&)) = 0;

  virtual Data<T>* Reduce(Data<T>*(*func)(Data<T>&,Data<T>&)) = 0;

  virtual ~Sequence<T>() = default;
};