#pragma once

#include "linkedlist.h"
#include "sequence.h"

template<class T>
class ListSequence : public Sequence<T> {
private:
  LinkedList<T> items;
public:
  ListSequence() : items() {}

  ListSequence(T *list, size_t size) : items(list, size) {}

  ListSequence(T* list, size_t size, size_t m_size) : items(list, size, m_size) {}

  ListSequence(const ListSequence<T> &list) : items(list.items) {}

  ////////////////////////////////////////////////////////////////////////////

  virtual int GetSize() const override { return items.GetSize(); }

  void Resize(int size) override { items.Resize(size); }

  virtual void Set(Data<T>* item, int index) override { items.Set(item, index); }

  Data<T>* Get(int index) const override { return items.Get(index); }

  Data<T>* GetFirst() const override { return items.GetFirst(); }

  Data<T>* GetLast() const override { return items.GetLast(); }

  Data<T>* operator[](int index) override { return items.Get(index); }

  void Append(Data<T> *item) override { items.Append(item); }

  void Prepend(Data<T> *item) override { items.Prepend(item); }

  void InsertAt(Data<T> *item, int index) override { items.InsertAt(item, index); }

  void Print() override { items.Print(); }

  Sequence<T> *Concat(Sequence<T> &list) override {
    auto *concatenated = new ListSequence<T>(*this);
    for (int i = 0; i < list.GetSize(); i++) {
      concatenated->Append(list[i]);
    }
    return concatenated;
  }

  Sequence<T> *GetSubsequence (int startIndex, int endIndex) override {
    auto *sub = new ListSequence<T>;
    for (int i = startIndex; i < endIndex; i++)
      sub->Append(items[i]);
    return sub;
  }

  Sequence<T> *Clone() override {
    auto *new_items = new ListSequence<T>();
    for (int i = 0; i < GetSize(); i++)
      new_items->Append(items[i]->copy());
    return new_items;
  }

  Sequence<T>* CopyNew(int size) override {
    auto *new_items = new ListSequence<T>();
    for (int i = 0; i < size; i++)
      new_items->Append(GetFirst()->copy_new());
    return new_items;
  };

  void Map(void(*func)(T&)) override{
      for (int i=0; i < GetSize(); i++)
        items[i]->map(func);
  };

  void Where(bool(*func)(T&)) override{
      items.where(func);
  };

  Data<T>* Reduce(Data<T>*(*func)(Data<T>&,Data<T>&)) override{
    return items.reduce(func);
  }

  virtual ~ListSequence<T>() = default;
};
