#pragma once

#include "dynamicarray.h"
#include "sequence.h"

template<class T>
class ArraySequence : public Sequence<T> {
private:
  DynamicArray<T> items;
public:
  ArraySequence() : items() {}

  ArraySequence(T *list, size_t size) : items(list, size) {}

  ArraySequence(T* matrix, size_t size, size_t m_size) : items(matrix, size, m_size) {}

  ArraySequence(const ArraySequence<T> &list) : items(list.items) {}

  ////////////////////////////////////////////////////////////////////////////

  virtual int GetSize() const override { return items.GetSize(); }

  void Resize(int size) override { items.Resize(size); }

  void Set(Data<T>* data, int index) override { items.Set(data, index); }

  Data<T>* Get(int index) const override { return items.Get(index); }

  Data<T>* GetFirst() const override { return items[0]; }

  Data<T>* GetLast() const override { return items[GetSize()-1]; }

  Data<T>* operator[](int index) override { return items.Get(index); }

  void Append(Data<T> *item) override {
    int size = items.GetSize();
    items.Resize(size + 1);
    items.Set(item, size);
  }

  virtual void Prepend(Data<T> *item) override {
    int size = items.GetSize();
    items.Resize(size + 1);
    for (int i = size-1; i > 0; i--)
      items.Set(items[i - 1], i);
    items.Set(item, 0);
  }

  virtual void InsertAt(Data<T> *item, int index) override {
    int size = items.GetSize();
    items.Resize(size + 1);
    for (int i = size; i > index; i--){
      items.Set(items[i - 1], i);
    }
    items.Set(item, index);
  }

  virtual void Print() override {
    int d = 0;
    for (int i = 0; i < GetSize(); i++) {
      items[i]->print();
      std::cout << "x^" << d << "\t";
      d++;
    }
    std::cout << std::endl;
  }

  Sequence<T> *Concat(Sequence<T> &list) override {
    auto *conc = new ArraySequence<T>;
    conc->items.Resize(GetSize() + list.GetSize());
    for (int i = 0; i < items.GetSize(); i++)
      conc->items.Set(items[i], i);
    for (int i = 0; i < list.GetSize(); i++)
      conc->items.Set(list[i], i + items.GetSize());
    return conc;
  }

  Sequence<T> *GetSubsequence(int startIndex, int endIndex) override {
    auto *sub = new ArraySequence<T>;
    sub->items.Resize(endIndex - startIndex);
    for (int i = startIndex; i < endIndex; i++)
      sub->items.Set(items[i], i - startIndex);
    return sub;
  }

  Sequence<T> *Clone() override {
    auto new_items = new ArraySequence<T>();
    new_items->Resize(GetSize());
    for (int i = 0; i < GetSize(); i++) 
      new_items->Set(items[i]->copy(), i);
    return new_items;
  }

  Sequence<T> *CopyNew(int size) override {
    auto *new_items = new ArraySequence<T>();
    new_items->Resize(size);
    for (int i = 0; i < size; i++)
      new_items->Set(GetFirst()->copy_new(), i);
    return new_items;
  }

  void Map(void(*func)(T&)) override{
    for (int i=0; i < GetSize(); i++)
      items[i]->map(func);
  };

  virtual void Where(bool(*func)(T&)) override{
      items.where(func);
  };

  Data<T>* Reduce(Data<T>*(*func)(Data<T>&,Data<T>&)) override{
    return items.reduce(func);
  }

  virtual ~ArraySequence<T>() = default;
};
