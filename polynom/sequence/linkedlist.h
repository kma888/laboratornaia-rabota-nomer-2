#pragma once

#include <iostream>

#include "data/number.h"
#include "data/matrix.h"

template<class T>
struct Item {
  Data<T>* value;
  Item<T> *prev = nullptr;
  Item<T> *next = nullptr;

  void Set(Data<T>* data) {value = data;}

  Item(Data<T>* value1, Item<T> *prev1, Item<T> *next1) : value(value1), prev(prev1), next(next1) {}

  Item(T value1, Item<T> *prev1, Item<T> *next1) : Item((Data<T>*) new Nums<T>(value1), prev1, next1) {}

  ~Item() = default;
};

template<class T>
class LinkedList {
private:
  Item<T> *first = nullptr;
  Item<T> *last = nullptr;
  int size;

public:
  LinkedList() : first(nullptr), last(nullptr), size(0) {}

  LinkedList(T *list, size_t size_) : LinkedList() {
    for (int i = 0; i < size_; i++) {
      auto p = (Data<T> *)new Nums<T>(list[i]);
      Append(p);
    }
  }

  LinkedList(T* list, size_t size, size_t m_size)
  {
    for (int i = 0; i < size; i++) {
      auto p = (Data<T> *)new Data_matrix<T>(list, i * m_size * m_size, m_size);
      Append(p);
    }
  }

  LinkedList(const LinkedList<T> &list) {
    size = list.GetSize();
    for (auto i = list.first; i != nullptr; i = i->next) {
      auto *item = new Item<T>(i->value, last, nullptr);
      if (first == nullptr)
        first = item;
      if (last != nullptr)
        last->next = item;
      last = item;
    }
  }

  /////////////////////////////////////////////////////////////////////////

  int GetSize() const {return size;}

  void Set(Data<T>* data, int index) { GetNode(index)->Set(data); }

  void Resize(size_t size_) {
    int old_size = GetSize();
    if (size_ <= old_size) {
      while (old_size != size_) {
        Item<T> *item = last;
        last = last->prev;
        delete item;
        old_size--;
      }
      last->next = nullptr;
    } else {
      while (old_size != size_) {
        auto *item = new Item<T>(0, last, nullptr);
        last->next = item;
        last = item;
        old_size++;
      }
    }
    size = size_;
  }

  void Append(Data<T> *item) {
    auto elem = new Item<T>(item, last, nullptr);
    if (first == nullptr)
      first = elem;
    if (last != nullptr)
      last->next = elem;
    last = elem;
    size++;
  }

  void Prepend(Data<T>* item){
    auto elem = new Item<T>(item, nullptr, first);
    if (first != nullptr)
      first->prev = elem;
    first = elem;
    if (last == nullptr)
      last = elem;
    size++;
  }

  void Print() {
    int d=0;
    for (auto i = first; i != nullptr; i = i->next){
      i->value->print();
      std::cout << "x^" << d << "\t";
      d++;
    }
    std::cout << std::endl;
  }

  void InsertAt(Data<T>* item, int index) {
    assert(index > 0 && index < GetSize());
    if (GetSize() == 0) {
      first = new Item<T>(item, nullptr, nullptr);
      last = new Item<T>(item, nullptr, nullptr);
    } else {
      if (index == 0) {
        auto *elem = new Item<T>(item, nullptr, first);
        first->prev = elem;
        first = elem;
      } else {
        Item<T> *i = first;
        for (int j = 0; j < index - 1; j++)
          i = i->next;
        auto *elem = new Item<T>(item, i, i->next);
        i->next = elem;
        if (elem->next == nullptr) {
          last = elem;
        } else {
          elem->next->prev = elem;
        }
      }
    }
  }

  void where(bool(*func)(T&)){
    auto tmp = first;
    while (tmp){
      if (func(tmp->value->Get())){
        tmp = tmp->next;
      }
      else{
        for (auto i=tmp; i->next!=NULL; i = i->next)
          i->value = i->next->value;
        delete last;
        last = last->prev;
        last->next = NULL;
        size--;
      }
    }
  }

  Data<T>* Get(size_t index) const {
    int j = 0;
    for (Item<T> *i = first; i != nullptr; i = i->next) {
      if (j == index)
        return i->value;
      j++;
    }
    return nullptr;
  }

  Item<T>* GetNode(size_t index) const {
    int j = 0;
    for (Item<T> *i = first; i != nullptr; i = i->next) {
      if (j == index)
        return i;
      j++;
    }
    return nullptr;
  }

  Data<T>* operator[](size_t index) const {
    return Get(index);
  }

  Data<T>* GetFirst() const { return first->value; }

  Data<T>* GetLast() const { return last->value; }

  Data<T>* reduce(Data<T>*(*func)(Data<T>&,Data<T>&)) {
    auto new_data = first->value->copy_new();
    for (auto i=first; i!=NULL; i=i->next){
      new_data = func(*new_data, *i->value);
    }
    return new_data;
  }

  ~LinkedList() {
    while (first != nullptr) {
      Item<T> *item = first;
      first = first->next;
      delete item;
    }
    first = nullptr;
    last = nullptr;
  }
};