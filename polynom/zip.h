#include "data/data.h"
#include "sequence/listsequence.h"
#include "sequence/arraysequence.h"
#include "iostream"


template<class T>
class ZIP {
  struct zip{
    Data<T>* first;
    Data<T>* second;

    zip() {first, second = nullptr;}
    zip(Data<T>* d1, Data<T>* d2) {first = d1; second = d2;}

    Data<T>* GetFirst() {return first;}
    Data<T>* GetSecond() {return second;}

    void ziprint(){
      std::cout << "(";
      if(first!=NULL){first->print();}
      std::cout << ", ";
      if(second!=NULL){second->print();}
      std::cout << ")" << "\t";
    }

    ~zip() = default;
  };

  zip** zipped;
  size_t size;

public:
  ZIP<T>(Sequence<T>* seq1, Sequence<T>* seq2){
    if(seq1->GetSize() >= seq2->GetSize()){
      zipped = new zip*[seq1->GetSize()];
      for (int i=0; i<seq1->GetSize(); ++i){
        if (i < seq2->GetSize()) { zipped[i] = new zip(seq1->Get(i), seq2->Get(i)); }
        else { zipped[i] = new zip(seq1->Get(i), NULL); }
      }
      size = seq1->GetSize();
    }
    else{
      zipped = new zip*[seq2->GetSize()];
      for (int i=0; i<seq2->GetSize(); ++i){
        if (i < seq1->GetSize()) { zipped[i] = new zip(seq1->Get(i), seq2->Get(i)); }
        else { zipped[i] = new zip(NULL, seq2->Get(i)); }
      }
      size = seq2->GetSize();
    }
  }

  Sequence<T>** unzip(){
    Sequence<T> *s1 = (Sequence<T>*) new ArraySequence<T>;
    Sequence<T> *s2 = (Sequence<T>*) new ArraySequence<T>;
    //Sequence<T> *s1 = (Sequence<T>*) new ListSequence<T>;
    //Sequence<T> *s2 = (Sequence<T>*) new ListSequence<T>;

    for (int i=0; i<size; ++i){
      if (zipped[i]->GetFirst()) { s1->Append( zipped[i]->GetFirst() ); }
      if (zipped[i]->GetSecond()) { s2->Append( zipped[i]->GetSecond() ); }
    }
    std::cout << "\nUnzipped sequences:\n";
    s1->Print(); s2->Print();
    Sequence<T>** unzip = new Sequence<T>*[2];
    return unzip;
  }

  void ZIPrint(){
    for (int i=0; i<size; ++i){
      zipped[i]->ziprint();
    }
  }
};