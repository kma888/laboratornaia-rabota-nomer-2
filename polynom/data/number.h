#pragma once
#include <iostream>
#include <complex>
#include "data.h"

template <class T> class Nums : public Data<T>
{
private:
  T *items;

public:
  Nums() { items = new T; }

  Nums(Data<T>& a) { *items = a.Get(); }

  Nums(T input) : Nums()  { *items = input; }

  Nums(T *input) : Nums() { *items = *input; }

////////////////////////////////////////////////////////////////////////////////

  std::string type() override { return "number"; }

  Data<T>* copy_new() override {
    auto c = new Nums<T>;
    return c;
  }

  Data<T>* copy() override {
    auto c = new Nums<T>(items);
    return c;
  }

  virtual void print() override { std::cout << *items; }

  T& Get() override { return *items; }

  void operator* (T scalar) override { *items *= scalar; }

  void operator* (Data<T>& a) override { *items *= a.Get(); }

  void operator+ (Data<T>& a) override { *items += a.Get(); }

  void map(void(*func)(T&)) override { func(*items); }

  ~Nums() = default;

////////////////////////////////////////////////////////////////////////////////
  virtual T*** Get_matrix() override {throw std::invalid_argument( "not a matrix" ); }

  virtual T& Get(size_t i, size_t j) override {throw std::invalid_argument( "not a matrix" ); }

  virtual void operator* (T*** matrix) override {throw std::invalid_argument( "not a matrix" ); };
};
////////////////////////////////////////////////////////////////////////////////

template<> void Nums<std::complex<int>>::print()
{
  std::cout << items->real() << " + " << items->imag() << "*i";
}

template<> void Nums<std::complex<float>>::print()
{
  std::cout << items->real() << " + " << items->imag() << "*i";
}