#pragma once
template <class T> class Data{
public:

  // ТОЛЬКО ДЛЯ ЧИСЕЛ
  virtual T& Get() = 0;
  //

  // ТОЛЬКО ДЛЯ МАТРИЦЫ
  virtual T*** Get_matrix() = 0;

  virtual T& Get(size_t i, size_t j) = 0;

  virtual void map(void(*func)(T&)) = 0;

  virtual std::string type() = 0;

  virtual Data<T>* copy_new() = 0;

  virtual Data<T>* copy() = 0;

  virtual void print() = 0;

  virtual void operator* (T scalar) = 0;

  virtual void operator* (T*** matrix) = 0;

  virtual void operator* (Data<T>& a) = 0;

  virtual void operator+ (Data<T>& a) = 0;
};