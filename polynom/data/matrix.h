#pragma once
#include <iostream>

#include "data.h"

template <class T>
class Data_matrix : public Data<T>
{
private:
  T*** m_data;
  size_t m_size;

public:
  Data_matrix() = default;

  Data_matrix(Data<T> &a) { m_data = a.Get_matrix(); }

  Data_matrix(size_t size)
  {
    m_data = new T**[size];
    m_size = size;
    for (int i=0; i<size; i++) {
      m_data[i] = new T*[size];
      for (int j = 0; j < size; j++) {
        m_data[i][j] = new T;
      }
    }
  }

  Data_matrix(T* matrix, int start, size_t size) : Data_matrix(size)
  {
    for (int i=0; i<size; i++)
      for (int j=0; j<size; j++)
        *m_data[i][j] = matrix[start + i*m_size + j];
  }

  Data_matrix(T* matrix, size_t size) : Data_matrix(matrix, 0, size) {}

////////////////////////////////////////////////////////////////////////////////

  T*** Get_matrix() override { return m_data; }

  std::string type() override { return "matrix"; };

  Data<T>* copy_new() override
  {
    auto c = new Data_matrix<T>(m_size);
    return c;
  }

  Data<T>* copy() override
  {
    auto copy = new Data_matrix<T>(m_size);

    for (int i=0; i<m_size; i++)
      for (int j=0; j<m_size; j++)
        *copy->m_data[i][j] = *m_data[i][j];

    return copy;
  }

  void print() override
  {
    for (int i=0; i<m_size; i++) {
      for (int j=0; j<m_size; j++){
        std::cout << *m_data[i][j] << " ";
      }
      std::cout << std::endl;
    }
  }

  T& Get(size_t i, size_t j) override { return *m_data[i][j]; }

  void operator* (T scalar) override
  {
    for (int i=0; i<m_size; i++)
      for (int j = 0; j < m_size; j++)
        *m_data[i][j] *= scalar;
  }

  virtual void operator* (T*** matrix) override
  {
    auto temp = new Data_matrix<T>(m_size);

    for(int i=0; i < m_size; i++)
      for(int j=0; j < m_size; j++)
        for(int k=0; k < m_size; k++)
          *temp->m_data[i][j] += Get(i, k) * *matrix[k][j];

    delete [] m_data;
    m_data = temp->m_data;
  }

  void operator* (Data<T>& a) override
  {
    if (a.type() == "number"){
      *this * a.Get();
    }

    if (a.type() == "matrix"){
      *this * a.Get_matrix();
    }
  }

  void operator+ (Data<T>& a) override
  {
    assert(a.type() == "matrix");
    for (int i=0; i < m_size; i++)
      for (int j=0; j < m_size; j++)
        *m_data[i][j] += a.Get(i, j);
  }

  void map(void(*func)(T&)) override {
    for (int i=0; i < m_size; i++)
      for (int j=0; j < m_size; j++)
        func(*m_data[i][j]);
  }

////////////////////////////////////////////////////////////////////////////////
  virtual T& Get() override {throw std::invalid_argument( "not a number" ); };
////////////////////////////////////////////////////////////////////////////////

  ~Data_matrix() { delete m_data; }
};