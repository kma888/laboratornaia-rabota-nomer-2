#include "poly.h"
#include <iostream>
using namespace std;

template <class T> void sqr(T &x) { x = x*x; }

template <class T> bool is_bigger_10(T& x) {return(x>10);}

template <class T> Data<T>* sum_function(Data<T>& a1, Data<T>& a2){
  assert(a1.type()=="number");
  return (Data<T>*) new Nums<T>(a1.Get() + a2.Get());
}

template<class T>
void operations(Polynomial<T> first, Polynomial<T> second){
  int n;
  cout << "Выберете операцию над многочленами: \n";
  cout << "1. Сложить\n";
  cout << "2. Перемножить\n";
  cout << "3. Умножение на скаляр\n";
  cout << "4. Композиция первого от второго\n";
  cout << "5. Композиция второго от первого\n";
  cout << "6. Взять квадрат каждого числа (map)\n";
  cout << "7. Оставить только числа большие 10 (where) //только для чисел\n";
  cout << "8. Найти сумму всех эл-тов (reduce)\n";
  cout << "9. Zip/unzip\n";
  cout << "10. Выйти\n";
  cin >> n;

  switch (n) {
  case 1:{
    first.AddPolynomial(second)->Print();
    operations(first, second);
  }
  case 2:{
    first.MulPolynomial(second)->Print();
    operations(first, second);
  }
  case 3:{
    int scalar;
    cout << "Введите скаляр:\t";
    cin >> scalar;
    first.MulScalar(scalar)->Print();
    second.MulScalar(scalar)->Print();
    operations(first, second);
  }
  case 4:{
    first.Composition(second)->Print();
    operations(first, second);
  }
  case 5:{
    second.Composition(first)->Print();
    operations(first, second);
  }
  case 6:{
    first.Map(sqr);
    first.Print();
    second.Map(sqr);
    second.Print();
    operations(first, second);
  }
  case 7:{
    first.Where(is_bigger_10);
    first.Print();
    second.Where(is_bigger_10);
    second.Print();
    operations(first, second);
  }
  case 8:{
    cout << "Сумма эл-тов первого многочлена:\t";
    first.Reduce(sum_function);
    first.Print();
    cout << "\n";
    cout << "Сумма эл-тов второго многочлена:\t";
    second.Reduce(sum_function);
    second.Print();
    operations(first, second);
  }
  case 9:{
    first.zip_unzip_poly(&second);
    operations(first, second);
  }
  case 10:{
    exit(0);
  }
  }
}

void interface(){
  cout << "Программа для работы с многочленами\n";
  cout << "С какими типами данных вы собираетесь работать? 1)числа 2)матрицы 3)выход\n";
  int n;
  cin>>n;
  switch (n) {
  case 1:{
    //=============================================================================================================//
    cout << "Введите кол-во эл-тов первого многочлена: ";
    int n;
    std::cin >> n;
    int *values = new int[n];
    int value;
    std::cout << "Введите " << n << "целых эл-тов первого многочлена :\n";
    for (int i = 0; i < n; ++i) {
      if (std::cin >> value)
        values[i] = value;
    }
    Polynomial<int> first (values, n, 'D');
    //=============================================================================================================//
    cout << "Введите кол-во эл-тов второго многочлена: ";
    cin >> n;
    int *values_ = new int[n];
    std::cout << "Введите " << n << "целых эл-тов первого многочлена :\n";
    for (int i = 0; i < n; ++i) {
      if (std::cin >> value)
        values_[i] = value;
    }
    Polynomial<int> second (values_, n, 'D');
    //=============================================================================================================//
    operations(first, second);
    interface();
  }
  case 2:{
    //=============================================================================================================//
    size_t size;
    cout << "Введите размер матрицы:\n";
    cin >> size;
    //=============================================================================================================//
    cout << "Введите кол-во эл-тов первого многочлена: ";
    int n;
    std::cin >> n;
    int *values = new int[n*size*size];
    int value;
    for (int i=0; i<n; ++i) {
      std::cout << "Введите " << i << " матрицу в виде строки:\n";
      std::cout << "Введите " << size*size << " целых эл-тов матрицы:\n";
      for (int j=0; j<size*size; ++j) {
        if (std::cin >> value)
          values[i*size*size + j] = value;
      }
    }
    Polynomial<int> first (values, size, n, 'D');
    //=============================================================================================================//
    cout << "Введите кол-во эл-тов второго многочлена: ";
    std::cin >> n;
    int *values_ = new int[n*size*size];
    for (int i=0; i<n; ++i) {
      std::cout << "Введите " << i << " матрицу в виде строки:\n";
      std::cout << "Введите " << size*size << " целых эл-тов матрицы:\n";
      for (int j=0; j<size*size; ++j) {
        if (std::cin >> value)
          values[i*size*size + j] = value;
      }
    }
    Polynomial<int> second (values, size, n, 'D');
    //=============================================================================================================//
    operations(first, second);
    interface();
  }
  case 3:{
    exit(0);
  }
  default:{cout << "Введите 1, 2 или 3!\n";}
  }
}

int main(){
  interface();
  return 0;
}