#include "gtest/gtest.h"

#include <iostream>
#include "time.h"
using namespace std;

#include "poly.h"

template <class T> void sqr(T &x) { x = x*x; }

template <class T> bool is_bigger_4(T& x) {return(x>4);}

template <class T> Data<T>* sum_function(Data<T>& a1, Data<T>& a2){
  assert(a1.type()=="number");
  return (Data<T>*) new Nums<T>(a1.Get() + a2.Get());
}

class poly_tests : public ::testing::Test{
public:
  virtual ~poly_tests() = default;

protected:

  virtual void SetUp1(){
    int N = 10000;
    int* arr = new int[N];
    int* arr2 = new int[N+1];

    for (int i=0; i<N; ++i){
      arr[i] = i;
      arr2[i] = i;
    }

    arr2[N] = -100;

    p0 = new Polynomial<int>(arr, N, 'D');
    p1 = new Polynomial<int>(arr, N, 'L');
    p2 = new Polynomial<int>(arr2, N+1, 'D');
    p3 = new Polynomial<int>(arr2, N+1, 'L');

  }

  virtual void SetUp2() {
    int arr1[] = {1,2,3,4,5,6,7,8};
    int arr2[] = {-1,-2,-3,-4,-5,-6,-7,-8};

    p4 = new Polynomial<int>(arr1, 2, 2, 'D');
    p5 = new Polynomial<int>(arr1, 2, 2, 'L');
    p6 = new Polynomial<int>(arr2, 2, 2, 'D');
    p7 = new Polynomial<int>(arr2, 2, 2, 'L');
  }

  virtual void SetUp0(){
    int arr1[] = {1,2,3,4};
    int arr2[] = {2,3,4,5,6};

    w0 = new Polynomial<int>(arr1, 4, 'D');
    w1 = new Polynomial<int>(arr1, 4, 'L');
    w2 = new Polynomial<int>(arr2, 5, 'D');
    w3 = new Polynomial<int>(arr2, 5, 'L');
  }

  virtual void SetUp3(){
    int arr[] = {1,2,3,4};

    z0 = new Polynomial<int>(arr, 4, 'D');
    z1 = new Polynomial<int>(arr, 4, 'L');
  }

  Polynomial<int> *p0;
  Polynomial<int> *p1;
  Polynomial<int> *p2;
  Polynomial<int> *p3;
  Polynomial<int> *p4;
  Polynomial<int> *p5;
  Polynomial<int> *p6;
  Polynomial<int> *p7;
  Polynomial<int> *w0;
  Polynomial<int> *w1;
  Polynomial<int> *w2;
  Polynomial<int> *w3;
  Polynomial<int> *z0;
  Polynomial<int> *z1;
};

TEST_F(poly_tests, poly_arr_sum0){
  SetUp1();
  int N = 10000;
  int* t_arr = new int[N+1];

  for (int i=0; i<N; ++i){
    t_arr[i] = 2*i;
  }

  t_arr[N] = -100;

  clock_t t1 = clock();
  auto P1 = p0->AddPolynomial(*p2);
  cout << (clock() - t1) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  clock_t t2 = clock();
  auto P2 = p2->AddPolynomial(*p0);
  cout << (clock() - t2) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  cout << endl;

  clock_t t3 = clock();
  auto P3 = p1->AddPolynomial(*p3);
  cout << (clock() - t3) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  clock_t t4 = clock();
  auto P4 = p3->AddPolynomial(*p1);
  cout << (clock() - t4) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  for (int i=0; i< P1->GetSize(); i++) {
    EXPECT_EQ(P1->Get(i)->Get(), t_arr[i]);
    EXPECT_EQ(P2->Get(i)->Get(), t_arr[i]);
    EXPECT_EQ(P3->Get(i)->Get(), t_arr[i]);
    EXPECT_EQ(P4->Get(i)->Get(), t_arr[i]);
  }
}

TEST_F(poly_tests, poly_matrix_sum){
  SetUp2();
  p4->AddPolynomial(*p6)->Print();
  p5->AddPolynomial(*p7)->Print();
}

TEST_F(poly_tests, poly_mult_arr1){
  SetUp0();

  int t_arr[] = {2,7,16,30,40,43,38,24};

  clock_t t1 = clock();
  auto W1 = w0->MulPolynomial(*w2);
  cout << (clock() - t1) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  clock_t t2 = clock();
  auto W2 = w2->MulPolynomial(*w0);
  cout << (clock() - t2) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  cout << endl;

  clock_t t3 = clock();
  auto W3 = w1->MulPolynomial(*w3);
  cout << (clock() - t3) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  clock_t t4 = clock();
  auto W4 = w3->MulPolynomial(*w1);
  cout << (clock() - t4) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  for (int i=0; i< W1->GetSize(); i++) {
    EXPECT_EQ(W1->Get(i)->Get(), t_arr[i]);
    EXPECT_EQ(W2->Get(i)->Get(), t_arr[i]);
    EXPECT_EQ(W3->Get(i)->Get(), t_arr[i]);
    EXPECT_EQ(W4->Get(i)->Get(), t_arr[i]);
  }
}

TEST_F(poly_tests, mul_scalar){
  SetUp1();

  int N = 10000;
  int* t_arr = new int[N];

  for (int i=0; i<N; ++i){
    t_arr[i] = 2*i;
  }

  clock_t t1 = clock();
  auto P1 = p0->MulScalar(2);
  cout << (clock() - t1) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  clock_t t2 = clock();
  auto P2 = p1->MulScalar(2);
  cout << (clock() - t2) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  for (int i=0; i< P1->GetSize(); i++) {
    EXPECT_EQ(P1->Get(i)->Get(), t_arr[i]);
    EXPECT_EQ(P2->Get(i)->Get(), t_arr[i]);
  }

}

TEST_F(poly_tests, poly_value_in_x_arr){
  SetUp1();
  //10000 * (10000 - 1) / 2 = 49995000
  clock_t t1 = clock();
  auto z1 = p0->CalculatingValue(1);
  cout << (clock() - t1) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  clock_t t2 = clock();
  auto z2 = p1->CalculatingValue(1);
  cout << (clock() - t2) / double (CLOCKS_PER_SEC) << "sec" << "\t";
  EXPECT_EQ(z1, 49995000);
  EXPECT_EQ(z2, 49995000);
}

TEST_F(poly_tests, poly_matrix_mult){
  SetUp2();
  //(A + Bx)(C + Dx) = A*C + (A*D + B*C) x + B*D x^2
  p4->MulPolynomial(*p6)->Print();
  p5->MulPolynomial(*p7)->Print();
}

TEST_F(poly_tests, poly_comp_arr1) {
  SetUp3();
  int t_arr[] = {10,40,120,292,519,768,924,816,576,256};

  clock_t t1 = clock();
  auto Z0 = z0->Composition(*z0);
  cout << (clock() - t1) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  clock_t t2 = clock();
  auto Z1 =z1->Composition(*z1);
  cout << (clock() - t2) / double (CLOCKS_PER_SEC) << "sec" << "\t";

  for (int i = 0; i < z0->GetSize(); i++) {
    EXPECT_EQ(Z0->Get(i)->Get(), t_arr[i]);
    EXPECT_EQ(Z1->Get(i)->Get(), t_arr[i]);
  }
}

TEST_F(poly_tests, poly_matrix_comp){
  SetUp2();
  p4->Composition(*p6)->Print();
  p5->Composition(*p7)->Print();
}

TEST_F(poly_tests, map_test1){
  SetUp0();
  w0->Map(sqr);
  w1->Map(sqr);
  w0->Print();
  w1->Print();
}

TEST_F(poly_tests, map_test2){
  SetUp2();
  p4->Map(sqr);
  p4->Print();
}

TEST_F(poly_tests, where_test1){
  //ТОЛЬКО ДЛЯ ЧИСЕЛ
  SetUp0();
  w2->Where(is_bigger_4);
  w3->Where(is_bigger_4);
  w2->Print();
  w3->Print();
}

TEST_F(poly_tests, reduce_tests){
  SetUp0();
  ASSERT_EQ(w0->Reduce(sum_function), 10);
  ASSERT_EQ(w1->Reduce(sum_function), 10);
}

TEST_F(poly_tests, zip_unzip_tests1){
  SetUp0();
  w0->zip_unzip_poly(w3);
}